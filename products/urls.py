from django.urls import path

from products.views import CategoryCreateView, list_all_products, show_product_detail

urlpatterns = [
    path("categories/new/", CategoryCreateView.as_view(), name="create_category"),
    path("<int:pk>/", show_product_detail, name="product_detail"),
    path("", list_all_products, name="product_list"),
]
