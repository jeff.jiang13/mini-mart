from django.shortcuts import render
from products.models import Category, Product
from django.views.generic.edit import CreateView


def show_product_detail(request, pk):
    product = Product.objects.get(pk=pk)
    context = {
        "product": product,
    }
    return render(request, "products/detail.html", context)


def list_all_products(request):
    products = Product.objects.all()
    context = {
        "products": products,
    }
    return render(request, "products/list.html", context)


class CategoryCreateView(CreateView):
    model = Category
    fields = ["name", "description"]
    template_name = "categories/create.html"
    success_url = "/products/categories/new"
