
python -m venv ./.venv
source ./.venv/bin/activate # on unix
python -m pip install --upgrade pip
pip install -r requirements.txt

./load_sample_data.sh
python manage.py createsuperuser
